package com.demo.at.numbersdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;

@Component
public class StringToNumberConverter {
    private static Logger LOGGER = LoggerFactory.getLogger(StringToNumberConverter.class);
    private static HashMap<String, Integer> numbers= new HashMap<String, Integer>();
    static {
        numbers.put("zero", 0);
        numbers.put("one", 1);
        numbers.put("two", 2);
        numbers.put("three", 3);
        numbers.put("four", 4);
        numbers.put("five", 5);
        numbers.put("six", 6);
        numbers.put("seven", 7);
        numbers.put("eight", 8);
        numbers.put("nine", 9);
        numbers.put("ten", 10);
        numbers.put("eleven", 11);
        numbers.put("twelve", 12);
        numbers.put("thirteen", 13);
        numbers.put("fourteen", 14);
        numbers.put("fifteen", 15);
        numbers.put("sixteen", 16);
        numbers.put("seventeen", 17);
        numbers.put("eighteen", 18);
        numbers.put("nineteen", 19);
        numbers.put("twenty", 20);
        numbers.put("thirty", 30);
        numbers.put("fourty", 40);
        numbers.put("fifty", 50);
        numbers.put("sixty", 60);
        numbers.put("seventy", 70);
        numbers.put("eighty", 80);
        numbers.put("ninety", 90);

        numbers.put("hundred", 100);
        numbers.put("thousand", 1000);
        numbers.put("million", 1000000);
        numbers.put("billion", 1000000000);
    }


    @Autowired
    public StringToNumberConverter() {
    }


    public  long convert(String input) {
        long result = 0;
        boolean isValidInput = true;

        //check input is valid first
        if(input != null && input.length()> 0) {
            input = input.replaceAll("-", " ");
            input = input.toLowerCase().replaceAll(" and", " ");
            String[] splittedParts = input.trim().split("\\s+");

            for (String str : splittedParts) {
                if (!numbers.containsKey(str)) {
                    isValidInput = false;
                    LOGGER.info("Invalid word found : " + str);
                    return -1;
                }
            }
            //calculate number
            if (isValidInput) {
                for (String str : splittedParts) {
                    long value = numbers.get(str);
                    if(value >= 100){
                      result *= value;
                    }else{
                        result += value;
                    }
                }

            }
        }
        return result;
    }

}
