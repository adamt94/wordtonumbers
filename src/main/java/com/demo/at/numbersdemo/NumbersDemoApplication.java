package com.demo.at.numbersdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


@SpringBootApplication
public class NumbersDemoApplication {
	private static Logger LOGGER = LoggerFactory.getLogger(NumbersDemoApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(NumbersDemoApplication.class, args);
		

	}

}
