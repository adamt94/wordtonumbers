package com.demo.at.numbersdemo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class ApplicationRunner {
    private Logger LOGGER = LoggerFactory.getLogger(ApplicationRunner.class);
    private Reader reader;
    private ApplicationContext context;
    private StringToNumberConverter converter;
    @Autowired
    public ApplicationRunner( StringToNumberConverter converter, Reader reader, ApplicationContext context) {
        this.converter = converter;
        this.reader = reader;
        this.context = context;
    }

    @PostConstruct
    public void run() throws IOException {
        boolean running = true;
        while(running){
            System.out.println("Enter any number in words: ");
            String value = reader.readCurrentLine();
            System.out.println(value);
            if(StringUtils.isEmpty(value) || value.equals("exit")){
                SpringApplication.exit(context,()->0);
                System.exit(0);
            }
            if(!StringUtils.isEmpty(value)){
               long result = converter.convert(value);
               System.out.println("You just entered: " + result);
            }
        }
    }
}
