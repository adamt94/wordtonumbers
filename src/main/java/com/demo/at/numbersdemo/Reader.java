package com.demo.at.numbersdemo;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Component
public class Reader {

    public String readCurrentLine() throws IOException {
        BufferedReader reader =  new BufferedReader(new InputStreamReader(System.in));
        String consoleInput = reader.readLine();
        return consoleInput;
    }
}
