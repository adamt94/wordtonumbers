package com.demo.at.numbersdemo;

import com.demo.at.numbersdemo.ApplicationRunner;
import com.demo.at.numbersdemo.Reader;
import com.demo.at.numbersdemo.StringToNumberConverter;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class ApplicationRunnerTest {

    private static final String NUMBER_STRING = "one hundred";
    private static final long NUMBER_INTEGER = 100;

    private ApplicationRunner applicationRunner;

    @Mock
    private Reader reader;

    @Mock
    private ApplicationContext context;

    @Mock
    private StringToNumberConverter converter;

    @Before
    public void setup() throws IOException {

        applicationRunner = new ApplicationRunner( converter, reader, context);


    }

    /**
     * Test a successful run
     * @throws IOException  - Shouldn't throw
     */
    @Test
    public void successTest() throws IOException {

        when(reader.readCurrentLine()).thenReturn(NUMBER_STRING, "");
        when(converter.convert(NUMBER_STRING)).thenReturn(NUMBER_INTEGER);

       // applicationRunner.run();
    }


}