package com.demo.at.numbersdemo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
public class StringToNumberConverterTest {

    private static final String NUMBER_STRING = "one hundred";
    private static final String NUMBER_STRING_BAD = "onehundred";
    private static final long NUMBER_INTEGER = 100;
    private StringToNumberConverter converter = new StringToNumberConverter();



    @Test
    public void testConvert_BadInput() {
        long result = converter.convert(NUMBER_STRING_BAD);
        assertEquals(-1,result);
    }
    /**
     * Test a successful run
     *
     */
    @Test
    public void testConvert_GoodInput() {
        long result = converter.convert(NUMBER_STRING);
        assertEquals(NUMBER_INTEGER,result);
    }


}
